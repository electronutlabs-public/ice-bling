/*

    Simulation:

    iverilog -o tb.out -s tb testbench.v simple_uart.v 
    vvp tb.out 
    Then open with GTKWave
*/

module tb ();
    reg clk = 0;
    wire [7:0] dataIn = 8'd9;
    wire tx;
    wire data_ready = 1;
    wire tx_busy;

    initial begin
        $dumpfile("testbench.vcd");
        $dumpvars;
        #10000
        $finish;
    end

    wire [3:0] xpos = 1;
    wire [3:0] ypos = 1;

    dot88 d88(
        .clk(clk),
        .xpos(xpos),
        .ypos(ypos),
        .row(row),
        .col(col)
    );

    always @ ( * ) begin
        #5 clk <= ~clk; // 100 ns clock
    end

endmodule
