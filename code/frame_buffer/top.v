/*
    top.v

    8x8 LED dot matrix

    electronut.in

*/

`default_nettype none

module top (
    input  clk,
    output LED2,

    output [7:0] anodes,
    output [7:0] cathodes
);

    // clk is at 12 MHz 
    reg [21:0] counter;

    // switcg pattern - counter
    reg [25:0] sw_counter;

    // LED pattern
    parameter PAT_LETTERS   = 2'b00;
    parameter PAT_CONWAY    = 2'b01;
    parameter PAT_RAIN      = 2'b10;
    reg [1:0] curr_patt;

    // BEGIN - init hack
    // iCE40 does not allow registers to initialised to 
    // anything other than 0
    // For workaround see:
    // https://github.com/YosysHQ/yosys/issues/103
    reg [7:0] resetn_counter = 0;
    wire  resetn = &resetn_counter;
    always @(posedge clk) 
        begin
            if (!resetn)
                resetn_counter <= resetn_counter + 1;
        end
    // END - init hack 

    wire [7:0] row;
    wire [7:0] col;

    // assign buffer based on curent pattern
    wire [63:0] w_fb = (curr_patt == PAT_LETTERS) ? w_fb_lett : 
                            ((curr_patt == PAT_CONWAY) ? w_fb_conway : w_fb_rain);

    // letters module
    wire [63:0] w_fb_lett;
    letters88 let (
        .resetn(resetn),
        .clk(clk),
        .fb(w_fb_lett)
    );

    // rain module
    wire [63:0] w_fb_rain;
    rain88 rn(
        .resetn(resetn),
        .clk(clk),
        .fb(w_fb_rain)
    );

    // Conway's GOL
    wire [63:0] w_fb_conway;
    conway88 c88 (
        .resetn(resetn),
        .clk(clk),
        .fb(w_fb_conway)
    );

    // instatiate dot88
    dot88 d88(
        .resetn(resetn),
        .clk(clk),
        .fb(w_fb),
        .row(row),
        .col(col)
    );
 
    // LED 
    reg L2;

    always @(posedge clk) 
    begin
        // initialise rot
        if (!resetn) 
            begin
                counter <= 0;
                curr_patt <= PAT_LETTERS;
                sw_counter <= 0;
            end
        else  
            begin
                
                // increment counters
                counter <= counter + 1;
                sw_counter <= sw_counter + 1;

                // switch pattern 
                if (!sw_counter)
                    curr_patt <= curr_patt + 1;

                // blink LED
                if (!counter) 
                    begin
                        L2 = ~L2;
                    end
            end        
    end

    // set LED output
    assign LED2 = L2;

    // set anodes and cathodes
    assign anodes = col;
    assign cathodes = row;
        
endmodule
