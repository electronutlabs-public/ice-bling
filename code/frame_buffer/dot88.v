/*

  dot88.v

*/

`default_nettype none

module dot88(
  input resetn,
  input clk,

  input [63:0] fb,

  output [7:0] row, // Anode
  output [7:0] col // Cathode
);

  reg [7:0] r = 0;
  reg [7:0] c = 0;

  reg [5:0] pcounter;
  wire [5:0] w_pcounter = pcounter;

  reg [3:0] xp;
  reg [3:0] yp;

  reg [11:0] d88_counter;
  
  always @ (posedge clk)
    begin
      
      // initialise 
      if (!resetn) 
          begin
              pcounter <= 0;
              d88_counter <= 0;
              xp <= 0;
              yp <= 0;
          end

      d88_counter <= d88_counter + 1;

      if (!d88_counter)
        begin
          pcounter <= pcounter + 1;
      
          // calculate position
          xp <= pcounter/8;
          yp <= pcounter - 8*(pcounter/8);  

          // update row/col
          c <= fb[pcounter-1] ? (8'd1 << (7 - xp)) : 8'd0;
          r <= ~(8'd1 << (7 - yp));
        end
        
    end

  assign {row, col} = {r, c};

endmodule

