# iCE Bling – LED Earrings with the Lattice iCE40UP5k FPGA

*We do not sell this as a product on our store*

[<img src="icebling.gif">](https://www.youtube-nocookie.com/embed/EQmQjjXrsqI?rel=0&amp;start=4)

This is a experimental project by Electronut Labs.

## What is iCE Bling ? 

iCE Bling is an Lattice iCE40UP5k FPGA based LED earrings. The earrings have 8x8 grid LEDs and are powered by CR2032 coin cell.

![iCE Bling](ice-bling-sm.jpg)

## The Design of iCE Bling

Here is link to the blog, [iCE Bling – Making LED Earrings with an FPGA](https://electronut.in/ice-bling-making-led-earrings-with-an-fpga/), on our website which will give you detailed information about iCE Bling..

# Hardware License 

All hardware design files in this repository are released under the Creative Commons License.

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>.

### Code Repository 

You can find the design files and code for the iCE Bling here.

[https://gitlab.com/electronutlabs-public/ice-bling](https://gitlab.com/electronutlabs-public/ice-bling)

## About Electronut Labs

**Electronut Labs** is an Embedded Systems company based in Bangalore, India. More 
information at our [website](https://electronut.in).
